from django.urls import path

from . import views

urlpatterns = [
    path("trunks", views.list_trunks),
    path("trunk/<str:trunk>", views.edit_trunk),
    path("trunk/<str:trunk>/delete", views.modal_trunk_delete),
    path("numbers", views.list_numbers),
    path("number/<str:number>", views.edit_number),
    path("number/<str:number>/delete", views.modal_number_delete),
    path("routings", views.list_routings),
    path("routings/create", views.modal_routing_create),
    path("routing/<str:routing>", views.edit_routing),
    path("routing/<str:routing>/delete", views.modal_routing_delete),
    path("cloudpbx", views.config_cloudpbxs),
    path("e-faxs", views.list_e_faxs),
    path("e-faxs/create", views.modal_e_faxs_create),
    path("e-fax/<str:e_fax>", views.edit_e_fax),
    path("e-fax/<str:e_fax>/delete", views.modal_e_fax_delete),
    path("blacklist", views.config_blacklist),
]

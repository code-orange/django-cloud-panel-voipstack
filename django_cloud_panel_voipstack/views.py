from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.template import loader
from django.utils.translation import gettext as _

from django_cloud_panel_main.django_cloud_panel_main.views import get_nav
from django_session_ldap_attributes.django_session_ldap_attributes.decorators import (
    admin_group_required,
)


@admin_group_required("TELEPHONY")
def list_trunks(request):
    template = loader.get_template("django_cloud_panel_voipstack/list_trunks.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Telephony")
    template_opts["content_title_sub"] = _("List Trunks")

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("TELEPHONY")
def edit_trunk(request, trunk):
    template = loader.get_template("django_cloud_panel_voipstack/edit_trunk.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Telephony")
    template_opts["content_title_sub"] = _("Edit Trunk")

    template_opts["trunk"] = trunk

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("TELEPHONY")
def modal_trunk_delete(request, trunk):
    template = loader.get_template(
        "django_cloud_panel_voipstack/modal_trunk_delete.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Telephony")
    template_opts["content_title_sub"] = _("Delete Trunk") + " " + trunk

    template_opts["trunk"] = trunk

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("TELEPHONY")
def list_numbers(request):
    template = loader.get_template("django_cloud_panel_voipstack/list_numbers.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Telephony")
    template_opts["content_title_sub"] = _("List Numbers")

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("TELEPHONY")
def edit_number(request, number):
    template = loader.get_template("django_cloud_panel_voipstack/edit_number.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Telephony")
    template_opts["content_title_sub"] = _("Edit Number")

    template_opts["number"] = number

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("TELEPHONY")
def modal_number_delete(request, number):
    template = loader.get_template(
        "django_cloud_panel_voipstack/modal_number_delete.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Telephony")
    template_opts["content_title_sub"] = _("Delete Number") + " " + number

    template_opts["number"] = number

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("TELEPHONY")
def list_routings(request):
    template = loader.get_template("django_cloud_panel_voipstack/list_routings.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Telephony")
    template_opts["content_title_sub"] = _("List Routings")

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("TELEPHONY")
def edit_routing(request, routing):
    template = loader.get_template("django_cloud_panel_voipstack/edit_routing.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Telephony")
    template_opts["content_title_sub"] = _("Edit Routing")

    template_opts["routing"] = routing

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("TELEPHONY")
def modal_routing_create(request):
    template = loader.get_template(
        "django_cloud_panel_voipstack/modal_routing_create.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Telephony")
    template_opts["content_title_sub"] = _("Create Routing")

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("TELEPHONY")
def modal_routing_delete(request, routing):
    template = loader.get_template(
        "django_cloud_panel_voipstack/modal_routing_delete.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Telephony")
    template_opts["content_title_sub"] = _("Delete Routing") + " " + routing

    template_opts["routing"] = routing

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("TELEPHONY")
def config_cloudpbxs(request):
    template = loader.get_template("django_cloud_panel_voipstack/config_cloudpbxs.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Telephony")
    template_opts["content_title_sub"] = _("Config CloudPBX")

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("TELEPHONY")
def list_e_faxs(request):
    template = loader.get_template("django_cloud_panel_voipstack/list_e_faxs.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Telephony")
    template_opts["content_title_sub"] = _("List E-Faxs")

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("TELEPHONY")
def edit_e_fax(request, e_fax):
    template = loader.get_template("django_cloud_panel_voipstack/edit_e_fax.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Telephony")
    template_opts["content_title_sub"] = _("Edit E-Fax")

    template_opts["e_fax"] = e_fax

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("TELEPHONY")
def modal_e_faxs_create(request):
    template = loader.get_template(
        "django_cloud_panel_voipstack/modal_e_faxs_create.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Telephony")
    template_opts["content_title_sub"] = _("Create E-Fax")

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("TELEPHONY")
def modal_e_fax_delete(request, e_fax):
    template = loader.get_template(
        "django_cloud_panel_voipstack/modal_e_fax_delete.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Telephony")
    template_opts["content_title_sub"] = _("Delete E-Fax") + " " + e_fax

    template_opts["e_fax"] = e_fax

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("TELEPHONY")
def config_blacklist(request):
    template = loader.get_template("django_cloud_panel_voipstack/config_blacklist.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Telephony")
    template_opts["content_title_sub"] = _("Config Blacklist")

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))
